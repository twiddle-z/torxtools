import typing as t

from . import xdgtools

__all__: t.List[str] = []

# call on instantiation
xdgtools.setenv()
