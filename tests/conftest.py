import os
from unittest.mock import patch

from pytest import fixture

from torxtools import xdgtools


@fixture(name="_noenv")
def fixture_noenv():
    # fmt: off
    k = patch.dict(
        os.environ, {
            "SPAM": "eggs",
            "HOME": "/home/jdoe",
            "USER": "jdoe",
            "SUBHOME": "$HOME/sub",
        }, clear=True,
    )
    # fmt: on

    k.start()
    xdgtools.setenv()
    yield
    k.stop()
